Droptica DevServer is virtualbox image with configured Ubuntu Server 12.04, GIT, Jenkins and sample Drupal project with PHPunit tests.

DOWNLOAD
https://drive.google.com/folderview?id=0B6FC57-wWF6cMWFhNGQwbW42V1k&usp=sharing

Informations about operating system:

OS: Ubunutu 12.04
Linux user name: droptica
Linux user password: drop
Network configuration: auto dhcp
Mysql root password: drop


Apache

Virtual hosts file: /etc/apache2/sites-enabled/jenkins

PHPMyAdmin: http://SERVERIP/phpmyadmin

Jenkins:

http://SERVERIP:8080

Jenkins jobs directory: /var/lib/jenkins/jobs/drupalproject_dev/workspace

Informations about Drupal sample project:

GIT repository location (bare): /var/git/drupalproject.git

Repository structure:

app - Drupal core and modules
conf - configuration files (Drupal settings, PHPunit settings)
database - Drupal database dump (after fresh installation)
docs - information how to run build script on local machine
scripts - build.sh script
Drupal administrator

login: admin

password: 123

PHPUnit tests examples

app/sites/all/modules/custom/application/tests/application.test

http://www.droptica.com/en/droptica-devserver
